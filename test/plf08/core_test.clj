(ns clojure00.core-test
  (:require [clojure.test :refer [deftest is testing]]
            [plf08.core :refer [codificar alfha]]
            [plf08.core :refer [codificar encriptar]]
            [plf08.core :refer [codificar cifrar]]
            [plf08.core :refer [codificar descifrar]]
            [plf08.core :refer [codificar cifrar-t]]
            [plf08.core :refer [codificar decifrar-t]]))

(deftest tabla-sustitución-test
  (testing "prueba 1"
    (is (= 20 (get (alfha "Ó") 0)))))


(deftest Polibio
  (testing "prueba Polibio"
    (is (= "R♞♕♝♖♝♚♜♖♝♝UIZ♛♛♛♛♗♝" (cifrar "RouterUIZ007")))
    (is (= "RouterUIZ007" (descifrar "R♞♕♝♖♝♚♜♖♝♝UIZ♛♛♛♛♗♝")))))

(deftest tras
  (testing "prueba tras"
    (is (= "urZ7oeI0RtU0" (cifrar-t "cba" "RouterUIZ007")))
    (is (= "RouterUIZ007"  (cifrar-t "cba" "urZ7oeI0RtU0")))))


