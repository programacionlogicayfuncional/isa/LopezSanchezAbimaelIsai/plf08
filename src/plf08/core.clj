(ns plf08.core
  (:require [clojure.string :as str])
  (:gen-class))
;==============================================================================
;||                                                                          ||
;||                            POLIBIO CUADRADO                              ||
;||                                                                          ||
;==============================================================================
(defn sutitución
  [c]
  (let [a ["♜♜" "♜♞" "♜♝" "♜♛" "♜♚" "♜♖" "♜♘" "♜♗" "♜♕" "♜♔"
           "♞♜" "♞♞" "♞♝" "♞♛" "♞♚" "♞♖" "♞♘" "♞♗" "♞♕" "♞♔"
           "♝♜" "♝♞" "♝♝" "♝♛" "♝♚" "♝♖" "♝♘" "♝♗" "♝♕" "♝♔"
           "♛♜" "♛♞" "♛♝" "♛♛" "♛♚" "♛♖" "♛♘" "♛♗" "♛♕" "♛♔"
           "♚♜" "♚♞" "♚♝" "♚♛" "♚♚" "♚♖" "♚♘" "♚♗" "♚♕" "♚♔"
           "♖♜" "♖♞" "♖♝" "♖♛" "♖♚" "♖♖" "♖♘" "♖♗" "♖♕" "♖♔"
           "♘♜" "♘♞" "♘♝" "♘♛" "♘♚" "♘♖" "♘♘" "♘♗" "♘♕" "♘♔"
           "♗♜" "♗♞" "♗♝" "♗♛" "♗♚" "♗♖"]
        d [\a \á \b \c \d \e \é \f \g \h \i \í \j \k \l \m \n \ñ \o
           \ó \p \q \r \s \t \u \ú \ü \v \w \x \y \z \0 \1 \2 \3 \4
           \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \=
           \> \? \@ \[ \\ \] \^ \_ \` \{ \| \} \~ \5 \6 \7 \8 \9 \space]]
    (if (= c "cifrado") (zipmap d a) (zipmap a d))))

;ejemplo
(sutitución "cifrado")
(sutitución "decifrado")

;CIFRAR.
(defn cifrar
  [s]
  (let [f (apply str (replace (sutitución "cifrado") s))]
    f))

;DESIFRAR
(defn descifrar
  [s]
  (let [f (into [] (take-nth 2 (map str s (drop 1 s))))]
    (apply str (replace (sutitución "decifrado")
                        f))))

;app
(defn apliacacion
  [x y z]
  (let [g (str/lower-case (slurp x))
        f (if (= y "cifrado") (cifrar g) (descifrar g))]
    (spit z f)))

(cifrar "RouterUIZ007")
(descifrar "R♞♕♝♖♝♚♜♖♝♝UIZ♛♛♛♛♗♝")
(apliacacion "kafka.txt" "cifrado" "res.txt")
(apliacacion "res.txt" "decifrado" "res2.txt")

;; (defn -main
;;   [& args y z]
;;   (if (empty? args)
;;     (println "No ingreso Nada")
;;     (apliacacion (apply str args) y z)))

;RECREACION DESIFRAR xD 
(apply str (replace (sutitución "decifrado") [" " "♜♔" "♞♕" "  " "♞♚" " " "♜♜"]))

(apply str
       (replace
        (sutitución "decifrado")
        ["H" "♜♔" "♞♕" "\n" "♞♚" "♜♜"]))
(vec
 (map vec
      (map flatten
           (partition-by (fn [x]
                           (cond
                             (= x \♜) true
                             (= x \♞) true
                             (= x \♝) true
                             (= x \♛) true
                             (= x \♚) true
                             (= x \♖) true
                             (= x \♘) true
                             (= x \♗) true
                             (= x \♕) true
                             (= x \♔) true
                             :else (str x)))
                         " ♜♔♞♕ ♞♚ ♜♜"))))

(defn respeto-xD
  [n]
  (vec (cond
         (< n 0) "negative"
         (> n 0) "positive"
         :else "zero")))



;==============================================================================
;||                                                                          ||
;||                  Transposición columnar simple:                          ||
;||                                                                          ||
;==============================================================================

(defn alfha
  [s]
  (vec (map
        (zipmap
         [\a \A \á \Á \b \B \c \C \d \D \e \e \é \É \f \F \g \G \h \H
          \i \I \í \Í \j \J \k \K \l \L \m \M \n \N \ñ \Ñ \o \O \ó \Ó
          \p \P \q \Q \r \R \s \S \t \T \u \U \ú \Ú \ü \Ü \v \V \w \W
          \x \X \y \Y \z \Z \0 \1 \2 \3 \4 \! \" \# \$ \% \& \' \( \)
          \* \+ \, \- \. \/ \: \; \< \= \> \? \@ \[ \\ \] \^ \_ \` \{
          \| \} \~ \5 \6 \7 \8 \9]
         [1 1 2 2 3 3 4 4 5 5 6 6 7 7 8 8 9 9 10 10 11 11 12 12 13 13
          14 14 15 15 16 16 17 17 18 18 19 19 20 20 21 21 22 22 23 23
          24 24 25 25 26 26 27 27 28 28 29 29 30 30 31 31 32 32 33 33
          34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53
          54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73
          74 75])
        s)))
(alfha "canción")
(alfha "CANCIÓN")
(alfha "cAnCiÓn")

;; Carga de texto
(slurp "kafka.txt")
;; Escritura de texto
(spit "res.txt" "")

;cifrar

(defn cifrar-t
  [clv word]
  (let [x (map-indexed
           (fn [kys xs] {(mod kys (count clv))  xs})
           word)
        y (mapv vec (vals (group-by keys x)))
        z (let [ys y]
            (for [x ys]
              (vec (map vec (map vals x)))))
        w (let [ys z]
            (for [x ys]
              (vec (flatten
                    (map conj x)))))]
    (apply str (flatten (map second (sort-by first (map vector (alfha clv) (vec w))))))))


;decifrar
(defn decifrar-t
  [clv word]
  (let [as (partition-all (/ (count word) (count clv)) word)
        bs (filterv char? (flatten (map flatten
                                        (sort-by first (map vector (sort-by second (map-indexed vector (alfha clv)))
                                                            (map vector (alfha (sort clv)) (vec as)))))))
        cs (vec (map vec (partition-all (/ (count word) (count clv)) bs)))
        ds (let [ys (vec cs)]
             (for [xs ys]
               (map-indexed (fn [id s] {id s}) xs)))
        es (mapv vec (vals (group-by keys (flatten ds))))
        fs (let [ys es]
             (for [xs ys]
               (vec (map vec xs))))
        gs (let [ys (vec fs)]
             (for [xs ys]
               (vec (flatten (flatten xs)))))
        hs (sort-by first gs)
        iss (let [ys (vec hs)]
              (for [xs ys]
                (filter char? xs)))]
    (apply str (flatten iss))))


;; Carga de texto
(slurp "res.txt")
;; Escritura de texto
(spit "res.txt" "")

;PRUEBAS
(cifrar-t "cba" (slurp "res.txt"))
(decifrar-t "cba" (cifrar-t "cba" (slurp "res.txt")))

;app
(defn apliacacion-2
  [x y v w]
  (let [g (slurp v)
        f (if (= x "cifrado") (cifrar-t y g) (decifrar-t y g))]
    (spit w f)))

(apliacacion-2 "cifrado" "cba"  "res.txt" "res2.txt")
(apliacacion-2 "decifrado" "cba"  "res2.txt" "res.txt")
;==============================================================================
;||                                                                          ||
;||                                 MAIN                                     ||
;||                                                                          ||
;==============================================================================

;APLIACION de AMBAS
(apliacacion "kafka.txt" "cifrado" "res.txt")
(apliacacion "res.txt" "decifrado" "res2.txt")
(apliacacion-2 "cifrado" "cba"  "res.txt" "res2.txt")
(apliacacion-2 "decifrado" "cba"  "res2.txt" "res.txt")


(defn -main 
  ([a b c d]
(= a "Polibio-cifrar")
(if (empty? d)
  (println "error en los argumentos, verifi que el orden")
  (apliacacion b c d)))
     ([a b c d e]
     (= a "cifradotcs")
     (if (empty? d)
  (println "error en los argumentos, verifi que el orden")
     (apliacacion-2 b c d e)))
  )

